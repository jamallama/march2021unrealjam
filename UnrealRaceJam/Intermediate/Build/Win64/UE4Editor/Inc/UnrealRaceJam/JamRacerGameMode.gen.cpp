// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UnrealRaceJam/JamRacerGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeJamRacerGameMode() {}
// Cross Module References
	UNREALRACEJAM_API UClass* Z_Construct_UClass_AJamRacerGameMode_NoRegister();
	UNREALRACEJAM_API UClass* Z_Construct_UClass_AJamRacerGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_UnrealRaceJam();
// End Cross Module References
	void AJamRacerGameMode::StaticRegisterNativesAJamRacerGameMode()
	{
	}
	UClass* Z_Construct_UClass_AJamRacerGameMode_NoRegister()
	{
		return AJamRacerGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AJamRacerGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AJamRacerGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_UnrealRaceJam,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AJamRacerGameMode_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "JamRacerGameMode.h" },
		{ "ModuleRelativePath", "JamRacerGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AJamRacerGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AJamRacerGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AJamRacerGameMode_Statics::ClassParams = {
		&AJamRacerGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AJamRacerGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AJamRacerGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AJamRacerGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AJamRacerGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AJamRacerGameMode, 2010580898);
	template<> UNREALRACEJAM_API UClass* StaticClass<AJamRacerGameMode>()
	{
		return AJamRacerGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AJamRacerGameMode(Z_Construct_UClass_AJamRacerGameMode, &AJamRacerGameMode::StaticClass, TEXT("/Script/UnrealRaceJam"), TEXT("AJamRacerGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AJamRacerGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
