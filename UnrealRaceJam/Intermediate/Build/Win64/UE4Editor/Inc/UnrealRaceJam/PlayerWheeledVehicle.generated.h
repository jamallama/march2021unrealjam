// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALRACEJAM_PlayerWheeledVehicle_generated_h
#error "PlayerWheeledVehicle.generated.h already included, missing '#pragma once' in PlayerWheeledVehicle.h"
#endif
#define UNREALRACEJAM_PlayerWheeledVehicle_generated_h

#define UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_SPARSE_DATA
#define UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_RPC_WRAPPERS
#define UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerWheeledVehicle(); \
	friend struct Z_Construct_UClass_APlayerWheeledVehicle_Statics; \
public: \
	DECLARE_CLASS(APlayerWheeledVehicle, AWheeledVehicle, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealRaceJam"), NO_API) \
	DECLARE_SERIALIZER(APlayerWheeledVehicle)


#define UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerWheeledVehicle(); \
	friend struct Z_Construct_UClass_APlayerWheeledVehicle_Statics; \
public: \
	DECLARE_CLASS(APlayerWheeledVehicle, AWheeledVehicle, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealRaceJam"), NO_API) \
	DECLARE_SERIALIZER(APlayerWheeledVehicle)


#define UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerWheeledVehicle(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerWheeledVehicle) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerWheeledVehicle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerWheeledVehicle); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerWheeledVehicle(APlayerWheeledVehicle&&); \
	NO_API APlayerWheeledVehicle(const APlayerWheeledVehicle&); \
public:


#define UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerWheeledVehicle(APlayerWheeledVehicle&&); \
	NO_API APlayerWheeledVehicle(const APlayerWheeledVehicle&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerWheeledVehicle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerWheeledVehicle); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerWheeledVehicle)


#define UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SpringArm() { return STRUCT_OFFSET(APlayerWheeledVehicle, SpringArm); } \
	FORCEINLINE static uint32 __PPO__Camera() { return STRUCT_OFFSET(APlayerWheeledVehicle, Camera); }


#define UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_12_PROLOG
#define UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_SPARSE_DATA \
	UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_RPC_WRAPPERS \
	UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_INCLASS \
	UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_SPARSE_DATA \
	UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_INCLASS_NO_PURE_DECLS \
	UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALRACEJAM_API UClass* StaticClass<class APlayerWheeledVehicle>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealRaceJam_Source_UnrealRaceJam_PlayerWheeledVehicle_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
