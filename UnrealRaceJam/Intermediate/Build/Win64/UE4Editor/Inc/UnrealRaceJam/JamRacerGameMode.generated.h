// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALRACEJAM_JamRacerGameMode_generated_h
#error "JamRacerGameMode.generated.h already included, missing '#pragma once' in JamRacerGameMode.h"
#endif
#define UNREALRACEJAM_JamRacerGameMode_generated_h

#define UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_SPARSE_DATA
#define UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_RPC_WRAPPERS
#define UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAJamRacerGameMode(); \
	friend struct Z_Construct_UClass_AJamRacerGameMode_Statics; \
public: \
	DECLARE_CLASS(AJamRacerGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealRaceJam"), NO_API) \
	DECLARE_SERIALIZER(AJamRacerGameMode)


#define UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAJamRacerGameMode(); \
	friend struct Z_Construct_UClass_AJamRacerGameMode_Statics; \
public: \
	DECLARE_CLASS(AJamRacerGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealRaceJam"), NO_API) \
	DECLARE_SERIALIZER(AJamRacerGameMode)


#define UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AJamRacerGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AJamRacerGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AJamRacerGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AJamRacerGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AJamRacerGameMode(AJamRacerGameMode&&); \
	NO_API AJamRacerGameMode(const AJamRacerGameMode&); \
public:


#define UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AJamRacerGameMode(AJamRacerGameMode&&); \
	NO_API AJamRacerGameMode(const AJamRacerGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AJamRacerGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AJamRacerGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AJamRacerGameMode)


#define UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_PRIVATE_PROPERTY_OFFSET
#define UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_12_PROLOG
#define UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_SPARSE_DATA \
	UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_RPC_WRAPPERS \
	UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_INCLASS \
	UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_SPARSE_DATA \
	UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_INCLASS_NO_PURE_DECLS \
	UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALRACEJAM_API UClass* StaticClass<class AJamRacerGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealRaceJam_Source_UnrealRaceJam_JamRacerGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
