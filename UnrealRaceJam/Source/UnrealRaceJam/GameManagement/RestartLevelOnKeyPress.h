// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "RestartLevelOnKeyPress.generated.h"

/**
 * 
 */
UCLASS()
class UNREALRACEJAM_API ARestartLevelOnKeyPress : public AGameStateBase
{
	GENERATED_BODY()
	
};
