// Copyright Epic Games, Inc. All Rights Reserved.

#include "UnrealRaceJam.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UnrealRaceJam, "UnrealRaceJam" );
