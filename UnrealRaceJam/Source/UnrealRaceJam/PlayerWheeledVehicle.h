// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WheeledVehicle.h"
#include "PlayerWheeledVehicle.generated.h"

/**
 * 
 */
UCLASS()
class UNREALRACEJAM_API APlayerWheeledVehicle : public AWheeledVehicle
{
	GENERATED_BODY()

public:

	APlayerWheeledVehicle();

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	/*throttle/steering*/
	void ApplyThrottle(float Val);
	void ApplySteering(float Val);

	/*Look around*/
	void LookUp(float Val);
	void Turn(float Val);

	/*Handbrake*/
	void OnHandBrakePressed();
	void OnHandBrakeReleased();

	/*Update in-air physics*/
	void UpdateInAirControl(float DeltaTime);

protected:

	/*Spring arm that will offset the camera*/
	UPROPERTY(Category = Camera, EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* SpringArm;

	/*Camera component that will be our viewpoint*/
	UPROPERTY(Category = Camera, EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* Camera;
};
